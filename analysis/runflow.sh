#########################################################################
# File Name: runflow.sh
# Author: Chenji Han
# mail: hanchenji16@mails.ucas.ac.cn
# Created Time: Tue 18 Feb 2020 02:32:20 PM CST
#########################################################################
#!/bin/bash

target=$1
tag=$2

inputFileName=sample/${target%.*}

mkdir -p plots
echo "loading file '${inputFileName}'"

echo "doing premary cutting on '${inputFileName}'"
root -l -b -q ./preFit.cxx'("'${inputFileName}'.root")'

echo " extracting the signal shape "
#root -l -b -q ./GetShape.cxx'("'${inputFileName}'_precut.root")'

echo " loading the fitting model "
root -l -b -q ./Fit_multiG.cxx'("'${inputFileName}'_precut.root","'${tag}'")'
#root -l -b -q ./Fit_shapeCov.cxx'("'${inputFileName}'_precut.root","sample/signalMC_precut.root_pdf.root","'${tag}'")'
#root -l -b -q ./Fit_recursive.cxx'("'${inputFileName}'_precut.root","sample/signalMC_precut.root_pdf.root","'${tag}'")'





















