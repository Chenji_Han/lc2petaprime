

#include"bes3plotstyle.C"
using namespace RooFit;
using namespace std;

void GetShape ( string inputName="~/scratchfs/DsMC/STV4/root/DsMCSTV5.root" ){
 

	gROOT->SetStyle("Plain");
	gStyle -> SetOptStat(0);

	TCut cut = "phiVeto2==0&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.09<DsGamma_m&&DsGamma_m<2.13)||(2.09<DsRecoil_m&&DsRecoil_m<2.13))";
	//TCut cut = "phiVeto2==0&&(mode1!=0||mode2!=0)&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.09<DsGamma_m&&DsGamma_m<2.13)||(2.09<DsRecoil_m&&DsRecoil_m<2.13))";

    TFile *inputFile = new TFile(inputName.c_str());
    cout<<"loading file "<<inputName<<endl;
	TTree *inputTree = (TTree*)inputFile->Get("output");

    string outputName("Ds_m_pdf_notatall.root");
    ifstream f(outputName.c_str());
    if( f.good() ){
        cout<<"the signal shape has been extracted"<<endl;
        return;
    }
 
	TFile *outputFile = new TFile(outputName.c_str(), "recreate");
	
	TTree *tree = (TTree*)inputTree->CopyTree(cut);
	
	RooRealVar Ds_m("Ds_m","Mass(Gev)",1.9,2.03);
	RooDataSet *data = new RooDataSet("data", "data", tree, RooArgSet(Ds_m));
	RooKeysPdf *sigPDF = new RooKeysPdf("sigPDF", "sigPDF", Ds_m, *data, RooKeysPdf::NoMirror,1.8);
  
	RooPlot* frame = Ds_m.frame();
    *sigPDF->plotOn(frame);

    TCanvas* c1 = new TCanvas("C1","C1",800,600);

	SetStyle();
	gStyle -> SetOptStat(0);
    gPad->SetBottomMargin(0.15);
    gPad->SetLeftMargin(0.15);
    FormatAxis(frame->GetXaxis());
    FormatAxis(frame->GetYaxis());
	frame->GetYaxis()->SetTitle("Events"); 
	frame->GetXaxis()->SetTitle("PDF of M(K^{+}K^{+}#pi^{-}#pi^{0})");
	//frame->GetYaxis()->SetTitleSize(0.04); 
	//frame->GetXaxis()->SetTitleSize(0.04);
	
    frame->Draw();

    string saveName = string("plots/Ds_m_pdf_notatall.png");
    c1->SaveAs(saveName.c_str());
	
	outputFile -> cd();
	sigPDF->Write();
	frame->Write();
	outputFile -> Close();
	
	return;
}
