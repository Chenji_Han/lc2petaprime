
#include"bes3plotstyle.C"

using namespace RooFit ;

void Extract_multiG(string inputName=string("./sample/signalMC5C.root"),string storename=string("Extraction5C") ){

    TFile *inputFile = new TFile(inputName.c_str());
    TTree *tr = (TTree*)inputFile->Get("Xi");
 	
	RooRealVar mXi("mXip4","",1.31,1.335);

    Int_t nentrs = tr->GetEntries();
      
    RooRealVar mean1("mean1","mean of gaussian",1.32132,1.32,1.323);
    RooRealVar mean2("mean2","mean of gaussian",1.32132,1.32,1.323);
    RooRealVar mean3("mean3","mean of gaussian",1.32132,1.32,1.323);
    RooRealVar mean4("mean4","mean of gaussian",1.32132,1.32,1.323);
    RooRealVar sigma1("sigma1","sigma core",0,0.01) ;
    RooRealVar sigma2("sigma2","sigma tail",0,0.01); 
    RooRealVar sigma3("sigma3","sigma tail",0,0.01); 
    RooRealVar sigma4("sigma4","sigma tail",0,0.01); 
    RooRealVar frac1("frac1","fraction",0,nentrs) ; 
    RooRealVar frac2("frac2","fraction",0,nentrs) ; 
    RooRealVar frac3("frac3","fraction",0,nentrs) ; 
    RooRealVar frac4("frac4","fraction",0,nentrs); 

    RooGaussian gauss1("gauss1","gauss",mXi,mean1,sigma1);
    RooGaussian gauss2("gauss2","gauss",mXi,mean2,sigma2);
    RooGaussian gauss3("gauss3","gauss",mXi,mean3,sigma3);
    RooGaussian gauss4("gauss4","gauss",mXi,mean4,sigma4);
    RooAddPdf model("gauss","gauss",RooArgList(gauss1,gauss2,gauss3,gauss4),RooArgList(frac1, frac2, frac3,frac4));
 
    RooDataSet data("data","data",tr,mXi);

    RooFitResult * result = model.fitTo(data,Save(kTRUE),Extended(kTRUE));//,Range(1.114,1.1175) for lmd

    ofstream outputTxtFile(".MultiGExtraction.txt",ios::out);
    if( !outputTxtFile ){
        cout<<" Error in opening output.txt " <<endl;
        return;
    }
   	outputTxtFile<<mean1.getValV()<<endl;
   	outputTxtFile<<mean2.getValV()<<endl;
   	outputTxtFile<<mean3.getValV()<<endl;
   	outputTxtFile<<mean4.getValV()<<endl;
   	outputTxtFile<<sigma1.getValV()<<endl;
   	outputTxtFile<<sigma2.getValV()<<endl;
   	outputTxtFile<<sigma3.getValV()<<endl;
   	outputTxtFile<<sigma4.getValV()<<endl;
   	outputTxtFile<<frac1.getValV()<<endl;
   	outputTxtFile<<frac2.getValV()<<endl;
   	outputTxtFile<<frac3.getValV()<<endl;
   	outputTxtFile<<frac4.getValV()<<endl;
    outputTxtFile.close();
 

    TCanvas* C = new TCanvas("C","C",800,600);
    SetStyle();

    gPad->SetLeftMargin(0.18);
    gPad->SetBottomMargin(0.13);
    RooPlot* frame = mXi.frame(1.31,1.335,100); 
	data.plotOn(frame,MarkerSize(0.6)); 
	model.plotOn(frame,Components(gauss1),LineColor(kRed),LineWidth(3));   
    model.plotOn(frame,Components(gauss2),LineStyle(kDashed),LineColor(kYellow),LineWidth(3));   
    model.plotOn(frame,Components(gauss3),LineStyle(kDashed),LineColor(kBlack),LineWidth(3));   
    model.plotOn(frame,Components(gauss4),LineStyle(kDashed),LineColor(kGreen),LineWidth(3));   
    model.plotOn(frame,LineWidth(3),LineColor(kBlue)) ;   

    FormatAxis(frame->GetYaxis());
    FormatAxis(frame->GetXaxis());
    frame->GetXaxis()->SetTitle("mass(#Xi^{-})/GeV");
    frame->GetYaxis()->SetTitle("Events");
    frame->GetYaxis()->SetLabelSize(0.03);
    frame->GetYaxis()->SetTitleSize(0.04);
    frame->GetXaxis()->SetLabelSize(0.03);
    frame->GetXaxis()->SetTitleSize(0.04);
	frame->Draw();
    
    string saveName = string("./plots/") + storename + string(".png");
    cout<<saveName<<endl;
	C->SaveAs(saveName.c_str());
    
    TCanvas* C2 = new TCanvas("C2","C2",800,600);
    gPad->SetLogy();
    frame -> Draw();
    string saveName2 = string("./plots/") + storename + string("_logy.png");
    cout<<saveName2<<endl;
    C2 -> SaveAs(saveName2.c_str());
    
	Int_t ndf = result->floatParsFinal().getSize();
	Double_t chisq = frame->chiSquare();
	Double_t reduced_chisq = frame->chiSquare(ndf);
	cout << "NDF:" << ndf << endl;
	cout << "chi square: " << chisq <<endl;
	cout << "reduced chi square: " << reduced_chisq << endl;
    cout << "fitted mass: "<<( mean1.getValV()*frac1.getValV() +
                               mean2.getValV()*frac2.getValV() +
                               mean3.getValV()*frac3.getValV() +
                               mean4.getValV()*frac4.getValV() ) / ( frac1.getValV() +
                                                                     frac2.getValV() +
                                                                     frac3.getValV() +
                                                                     frac4.getValV() ) ;

    double N = frac1.getValV() + frac2.getValV() + frac3.getValV() + frac4.getValV();
    double variance2 = frac1.getValV()*sigma1.getValV()*sigma1.getValV()/(N*N) +
                       frac2.getValV()*sigma2.getValV()*sigma2.getValV()/(N*N) +
                       frac3.getValV()*sigma3.getValV()*sigma3.getValV()/(N*N) +
                       frac4.getValV()*sigma4.getValV()*sigma4.getValV()/(N*N) ; 
    cout<<" +/- "<<sqrt(variance2)<<endl;






}
