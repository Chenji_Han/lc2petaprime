/*************************************************************************
    > File Name: drawTruth.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: 2020年07月17日 星期五 18时41分25秒
 ************************************************************************/

#include "./DrawHist.cxx"


void drawTruth(){

    TFile inputFile("/afs/ihep.ac.cn/users/h/hancj/scratchfs/DsMC/STV4/root/Ds.root");
    TTree* inputTree = (TTree*) inputFile.Get("output");
    TTree* inputTree_truth = (TTree*) inputFile.Get("output_truth");
  
    int mode1;
    int mode2;
    double DeltaE, Chisq1C;
    int phiVeto2;
    inputTree -> SetBranchAddress("mode1",&mode1);
    inputTree -> SetBranchAddress("mode2",&mode2);
    inputTree -> SetBranchAddress("DeltaE",&DeltaE);
    inputTree -> SetBranchAddress("Chisq1C",&Chisq1C);
    inputTree -> SetBranchAddress("phiVeto2",&phiVeto2);

    double K1_p_truth;
    double K2_p_truth;
    double Pi_p_truth;
    double otherKp_p_truth;
    double otherKm_p_truth;
    double otherPip_p_truth;
    double otherPim_p_truth;
    inputTree_truth -> SetBranchAddress("K1_p_truth",&K1_p_truth);
    inputTree_truth -> SetBranchAddress("K2_p_truth",&K2_p_truth);
    inputTree_truth -> SetBranchAddress("Pi_p_truth",&Pi_p_truth);
    inputTree_truth -> SetBranchAddress("otherKp_p_truth",&otherKp_p_truth);
    inputTree_truth -> SetBranchAddress("otherKm_p_truth",&otherKm_p_truth);
    inputTree_truth -> SetBranchAddress("otherPip_p_truth",&otherPip_p_truth);
    inputTree_truth -> SetBranchAddress("otherPim_p_truth",&otherPim_p_truth);

    TH1D H1("H1","",100,0,1.2);
    TH1D H2("H2","",100,0,1.2);
    TH1D H3("H3","",100,0,1.2);
    TH1D H4("H4","",100,0,1.2);
    TH1D H5("H5","",100,0,1.2);
    TH1D H6("H6","",100,0,1.2);
    TH1D H7("H7","",100,0,1.2);
    

    int event=0;
    for(int i=0; i<inputTree_truth->GetEntries(); i++){

        inputTree_truth->GetEntry(i);
        inputTree->GetEntry(i);

        if(!(phiVeto2==0&&(mode1!=0||mode2!=0)&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30)) continue;
        event++;
        
        H1.Fill(K1_p_truth);
        H2.Fill(K2_p_truth);
        H3.Fill(Pi_p_truth);
        H4.Fill(otherKp_p_truth);
        H5.Fill(otherKm_p_truth);
        H6.Fill(otherPip_p_truth);
        H7.Fill(otherPim_p_truth);

    }


        
    draw_com7(&H1, "K^{+} 1",
              &H2, "K^{+} 2",
              &H3, "#pi^{-}",
              &H4, "other K^{+}",
              &H5, "other K^{-}",
              &H6, "other #pi^{+}",
              &H7, "other #pi^{-}",
              "truthP_com");

    cout<<endl<<event<<endl;

}
