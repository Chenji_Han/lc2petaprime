/*************************************************************************
    > File Name: cutflow.cxx
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Mon 06 Jan 2020 02:34:08 PM CST
 ************************************************************************/

#include"bes3plotstyle.C"
#include<iostream>
using namespace std;


void cutflowXi(){

//    TFile* inputFile = new TFile("Inclusive2009V1.root");
    TFile* inputFile = new TFile("sample/signalMC.root");
    //TFile* inputFile = new TFile("mXiShape.root");
    TTree* inputTreeXi = (TTree*) inputFile -> Get("Xi");
    TTree* inputTreeXiTruth = (TTree*) inputFile -> Get("XiTruth");

    Int_t Ntrack;
    Int_t NProtonP;
    Int_t NProtonM;
    Int_t NPionP;
    Int_t NPionM;

	double mXi;
	double mXibar;
	double mXiRecoil;
	double mLbd;
	double DE_Xi;

	double X2_vf_Lbd;
	double X2_vf_Xi;
	double decayLength_Lbd;
	double decayError_Lbd;
	double decayLength_Xi;
	double decayError_Xi;

	//inputTreeXi -> SetBranchAddress("NProtonP",&NProtonP);
	//inputTreeXi -> SetBranchAddress("NProtonM",&NProtonM);
	//inputTreeXi -> SetBranchAddress("NPionM",&NPionM);
	//inputTreeXi -> SetBranchAddress("NPionP",&NPionP);
	//inputTreeXi -> SetBranchAddress("Ntrack",&Ntrack);

	inputTreeXi -> SetBranchAddress("ProtonPZ",&mXibar);
	//inputTreeXi -> SetBranchAddress("mXi",&mXi);
	inputTreeXi -> SetBranchAddress("mXip4",&mXi);
	inputTreeXi -> SetBranchAddress("mXiRecoil",&mXiRecoil);
	inputTreeXi -> SetBranchAddress("mLbd",&mLbd);
	inputTreeXi -> SetBranchAddress("DE_Xi",&DE_Xi);
	inputTreeXi -> SetBranchAddress("X2_vf1_Lbd",&X2_vf_Lbd);
	inputTreeXi -> SetBranchAddress("X2_vf1_Xi",&X2_vf_Xi);
	inputTreeXi -> SetBranchAddress("decayLength_Lbd",&decayLength_Lbd);
	inputTreeXi -> SetBranchAddress("decayError_Lbd",&decayError_Lbd);
	inputTreeXi -> SetBranchAddress("decayLength_Xi",&decayLength_Xi);
	inputTreeXi -> SetBranchAddress("decayError_Xi",&decayError_Xi);
	
	TH1I* HNProtonP = new TH1I("NProtonP","",10,0,10);	
	TH1I* HNProtonM = new TH1I("NProtonM","",10,0,10);	
	TH1I* HNPionM = new TH1I("NPionM","",10,0,10);	
	TH1I* HNPionP = new TH1I("NPionP","",10,0,10);	
	TH1I* HNtrack = new TH1I("Ntrack","",10,0,10);	
	TH1D* HdecayLength_Lbd =  new TH1D("decayLength_Lbd","",100,-10,30);	
	TH1D* HdecayLength_Xi = new TH1D("decayLength_Xi","",100,-10,30);	
	TH1D* HmXi = new TH1D("mass Xi","",100,1.28,1.36);	
	//TH1D* HmXibar = new TH1D("mass Xibar","",100,1.28,1.36);	
	TH1D* HLbd = new TH1D("mass Lbd","",100,1.08,1.18);	
	TH1D* HDE = new TH1D("DE","",100,-0.08,0.08);	
	TH1D* HmXiRecoil = new TH1D("mass Xi recoil","",100,1.28,1.36);	
	TH2D* mXi_mXiRecoil = new TH2D("mXi_mXiRecoil","",100,1.28,1.36,100,1.25,1.38);
	TH2D* mXi_DE = new TH2D("mXi_DE","",100,1.28,1.36,100,-0.04,0.04);

	int number = 0;

	for(long int ievt=0;ievt<inputTreeXi->GetEntries();ievt++){
		
		inputTreeXi->GetEntry(ievt);
//		inputTreeXibar->GetEntry(ievt);

		if(ievt%50000==0) cout<<"---processing "<<ievt<<" events"<<endl;
		
		HDE -> Fill(DE_Xi);
		HLbd -> Fill(mLbd);
	//	if(!(X2_vf_Lbd<100)) continue;
	//	if(!(X2_vf_Xi<100)) continue;
	//	if(!(decayLength_Lbd>0)) continue;
	//	if(!(decayLength_Xi>0)) continue;
		if(!(mLbd>1.111&&mLbd<1.121)) continue;
//		if(!(Ntrack==6)) continue;
		//if(!(mXi>1.28&&mXi<1.36&&mXibar>1.28&&mXibar<1.36)) continue;
	//	if(!(DE_Xi>-0.02&&DE_Xi<0.02)) continue;
		//if(!(mXi>1.28&&mXiRecoil<1.36)) continue;
	//	if(!(mXiRecoil>1.3&&mXiRecoil<1.34)) continue;
        
		number++;

		//HNProtonP -> Fill(NProtonP);
		//HNProtonM -> Fill(NProtonM);
		//HNPionM ->   Fill(NPionM);
		//HNPionP ->   Fill(NPionP);
		//HNtrack ->   Fill(Ntrack);

		HmXiRecoil -> Fill(mXiRecoil);
		HdecayLength_Lbd -> Fill(decayLength_Lbd);
		HdecayLength_Xi -> Fill(decayLength_Xi);
		HmXi -> Fill(mXi);
		//HmXibar -> Fill(mXibar);
		mXi_mXiRecoil -> Fill(mXi,mXiRecoil);
		mXi_DE -> Fill(mXi,DE_Xi);
	}

	SetStyle();
	gStyle -> SetOptStat(0);
	TCanvas C1;
	mXi_mXiRecoil -> Draw("COLZ");
    FormatAxis(mXi_mXiRecoil->GetYaxis());
    FormatAxis(mXi_mXiRecoil->GetXaxis());
	mXi_mXiRecoil -> GetXaxis() -> SetTitle("mass(#Xi^{-})");
	mXi_mXiRecoil -> GetYaxis() -> SetTitle("mass(#Xi^{-}_{recoil})");
	C1.SaveAs("./plots/mass_recoil.png");

return;

    Format(HmXi);
    //Format(HmXibar);
    Format(HmXiRecoil);
    Format(HdecayLength_Xi);
    Format(HdecayLength_Lbd);
    Format(HLbd);
    Format(HDE);

    Format(HNProtonP);
    Format(HNProtonM);
    Format(HNPionM);
    Format(HNPionP);
    Format(HNtrack);

	TCanvas C2;
	HmXi -> GetXaxis() -> SetTitle("mass(#Xi^{-})/GeV");
	//HmXi -> GetYaxis() -> SetTitle("Events");
	HmXi -> Draw("hist");
	C2.SaveAs("./plots/massXi.png");

	TCanvas C3;
	HmXiRecoil -> GetXaxis() -> SetTitle("mass(#Xi^{-}_{recoil})/GeV");
//	HmXiRecoil -> GetYaxis() -> SetTitle("Events");
	HmXiRecoil -> Draw("hist");
	C3.SaveAs("./plots/massXiRecoil.png");

	TCanvas C4;
	HLbd -> GetXaxis() -> SetTitle("mass(#Lambda)/GeV");
	//HLbd -> GetYaxis() -> SetTitle("Events");
	HLbd -> Draw("hist");
	C4.SaveAs("./plots/massLbd.png");

	TCanvas C5;
	HDE -> GetXaxis() -> SetTitle("#DeltaE/GeV");
	//HDE -> GetYaxis() -> SetTitle("Events");
	HDE -> Draw("hist");
	C5.SaveAs("./plots/DE.png");

	TCanvas C6;
	HdecayLength_Lbd -> GetXaxis() -> SetTitle("deacy length of #Lambda/cm");
	//HdecayLength_Lbd -> GetYaxis() -> SetTitle("Events");
	HdecayLength_Lbd -> Draw("hist");
	C6.SaveAs("./plots/decayLength_Lbd.png");

	TCanvas C7;
	HdecayLength_Xi -> GetXaxis() -> SetTitle("deacy length of #Xi/cm");
	//HdecayLength_Xi -> GetYaxis() -> SetTitle("Events");
	HdecayLength_Xi -> Draw("hist");
	C7.SaveAs("./plots/decayLength_Xi.png");

	TCanvas C8;
	mXi_DE -> Draw("COLZ");
    FormatAxis(mXi_DE->GetYaxis());
    FormatAxis(mXi_DE->GetXaxis());
	mXi_DE -> GetYaxis() -> SetTitle("#deltaE");
	mXi_DE -> GetXaxis() -> SetTitle("mass(#Xi)/GeV");
	C8.SaveAs("./plots/mass_DE.png");

    TCanvas C9;
	HNProtonP -> GetXaxis() -> SetTitle("number of p^{+}");
	HNProtonP -> Draw("hist");
	C9.SaveAs("./plots/NProtonP.png");

    TCanvas C10;
	HNProtonM -> GetXaxis() -> SetTitle("number of p^{-}");
	HNProtonM -> Draw("hist");
	C10.SaveAs("./plots/NProtonM.png");

    TCanvas C11;
	HNPionM -> GetXaxis() -> SetTitle("number of #pi^{-}");
	HNPionM -> Draw("hist");
	C11.SaveAs("./plots/NPionM.png");

    TCanvas C12;
	HNPionP -> GetXaxis() -> SetTitle("number of #pi^{+}");
	HNPionP -> Draw("hist");
	C12.SaveAs("./plots/NPionP.png");

    TCanvas C13;
	HNtrack -> GetXaxis() -> SetTitle("number of charged tracks");
	HNtrack -> Draw("hist");
	C13.SaveAs("./plots/Ntrack.png");
//
//    TCanvas C14;
//	HmXibar -> GetXaxis() -> SetTitle("mass(#bar{#Xi}^{+})");
//	HmXibar -> Draw("hist");
//	C14.SaveAs("./plots/mXibar.png");



	cout<<"reconstructed events = "<<number<<endl;


}

















