/*************************************************************************
    > File Name: addNewBranch.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Sat 18 Apr 2020 03:05:12 PM CST
 ************************************************************************/



void phiVeto(){

    TFile inputFile("./test.root","update");
    TTree* inputTree = (TTree*) inputFile.Get("output");

    double phi[20][5];

    inputTree -> SetBranchAddress("KpKm",phi);

    TH1D H("H","",100,1,1.04);

    int phiVeto;
    TBranch* newBranch = inputTree -> Branch("phiVeto2",&phiVeto);
    
    const double mDs = 1.9683; 

    for(int ievt=0; ievt<inputTree->GetEntries(); ievt++ ){
        inputTree -> GetEntry(ievt);
        if(ievt%2000==0) cout<<"process..."<<ievt<<endl;

        phiVeto = 0;
        for(int i=0;i<20;i++){
            if(1.00<phi[i][0]&&phi[i][0]<1.04){
                phiVeto = 1;
                break;
            }
        }
        newBranch -> Fill();

    }

    //TCanvas C;
    //H.Draw("hit");
    //C.SaveAs("phiveto.png");

    inputTree -> Write();


}

