#########################################################################
# File Name: clean.sh
# Author: Chenji Han
# mail: hanchenji16@mails.ucas.ac.cn
# Created Time: Wed 04 Mar 2020 10:25:54 AM CST
#########################################################################
#!/bin/bash


rm -f ./sample/M*precut*root
rm -f ./sample/M*precut*png
rm -f ./sample/runflow.sh.*


