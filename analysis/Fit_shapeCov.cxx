#include"bes3plotstyle.C"
using namespace RooFit;
using namespace std;

void Fit_shapeCov(string inputName,string inputShapeName,string flag){


	TFile *inputFile = new TFile(inputName.c_str());
    TTree *Tree = (TTree*)inputFile->Get("Xi");
 	
	RooRealVar mXi("mXip4","",1.31,1.335);

    RooRealVar gmean("mean","",8e-4,-0.01,0.01);
    RooRealVar gsigma("sigma","",0,0.01);
    RooGaussian covg("covg","the gauss to cov mc shape",mXi,gmean,gsigma);

    cout<<"loading the shape p.d.f.  "<<inputShapeName<<endl;
	TFile * inputShape = new TFile(inputShapeName.c_str());
	RooKeysPdf* mcShape = inputShape->Get("sigPDF");	
			
	RooFFTConvPdf sigPdf("sigPdf","",mXi,*mcShape,covg);

    RooRealVar a1("a1","",-2,2);
    RooRealVar a2("a2","",-1,1);
    //RooPolynomial bkgPdf("bkgpdf","Argus PDF",mXi,RooArgList(a1));
    RooPolynomial bkgPdf("bkgpdf","Argus PDF",mXi,RooArgList(a1,a2));

	int evtNums = Tree->GetEntries();
	//int evtNums = Tree->GetEntries(cut);
    RooRealVar nsig("nsig","#signal Events",0,evtNums);
    RooRealVar nbkg("nbkg","#background Events",0,evtNums);
    
	RooAddPdf model("model","sig+bkg",RooArgList(sigPdf,bkgPdf),RooArgList(nsig,nbkg));
    RooDataSet data = RooDataSet("data","data after cut",Tree,mXi);

	RooFitResult *result = model.fitTo(data,Save(kTRUE),Extended());

    TCanvas* C = new TCanvas("C","C",800,600);
    SetStyle();

    gPad->SetLeftMargin(0.18);
    gPad->SetBottomMargin(0.13);
    RooPlot* frame = mXi.frame(1.31,1.335,100); 
	data.plotOn(frame,MarkerSize(0.6)); 
	//data.plotOn(frame,MarkerColor(12),MarkerStyle(2)); 
	model.plotOn(frame,Components(sigPdf),LineColor(kRed),LineWidth(3));   
    model.plotOn(frame,Components(bkgPdf),LineStyle(kDashed),LineColor(kYellow),LineWidth(3));   
    model.plotOn(frame,LineWidth(3),LineColor(kBlue)) ;   

    FormatAxis(frame->GetYaxis());
    FormatAxis(frame->GetXaxis());
    frame->GetXaxis()->SetTitle("mass(#Xi^{-})/GeV");
    frame->GetYaxis()->SetTitle("Events");
    frame->GetYaxis()->SetLabelSize(0.03);
    frame->GetYaxis()->SetTitleSize(0.04);
    frame->GetXaxis()->SetLabelSize(0.03);
    frame->GetXaxis()->SetTitleSize(0.04);
	frame->Draw();

    
    string saveName = string("./plots/") + flag + string(".png");
    cout<<saveName<<endl;
	C->SaveAs(saveName.c_str());
    
    TCanvas* C2 = new TCanvas("C2","C2",800,600);
    gPad->SetLogy();
    frame -> Draw();
    string saveName2 = string("./plots/") + flag + string("_logy.png");
    cout<<saveName2<<endl;
    C2 -> SaveAs(saveName2.c_str());
    
    ofstream outputTxtFile("output.txt",ios::app);
    if( !outputTxtFile ){
        cout<<" Error in opening output.txt " <<endl;
        return;
    }

    string tag = inputName;
  
    //string line;
    //if( !std::getline( outputTxtFile, line ) ){
    //    outputTxtFile<<"mass  mean  mean_error  sigma  sigma_error"<<endl;
    //}

	RooDataSet* afterFit = sigPdf.generate(mXi,nsig.getValV());
	RooRealVar* sigmaSig = afterFit -> rmsVar(mXi);
	RooRealVar* meanSig = afterFit -> meanVar(mXi);

   	outputTxtFile<<tag<<"  "<<gmean.getValV()+1.32132<<"  "<<gmean.getError()<<"  ";
    
   	outputTxtFile<<scientific<<gsigma.getValV()<<"  "<<gsigma.getError()<<"  ";

   	outputTxtFile<<scientific<<meanSig->getValV()<<"  "<<meanSig->getError()<<"  ";

	outputTxtFile<<scientific<<sigmaSig->getValV()<<"  "<<sigmaSig->getError()<<"  ";

    outputTxtFile<<nsig.getValV()<<"  "<<nbkg.getValV()<<endl;
 
//	cout<<mass<<"  "<<gmean.getValV()+1.32132<<"  "<<gmean.getError();
//	cout<<"  "<<gsigma.getValV()<<"  "<<gsigma.getError()<<endl;


    delete inputFile;
    delete inputShape;
    delete C;
}
















