#ifndef Physics_Analysis_Lc2PEtaprime_H
#define Physics_Analysis_Lc2PEtaprime_H 

#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/NTuple.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/PropertyMgr.h"
#include "VertexFit/IVertexDbSvc.h"
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/ISvcLocator.h"

#include "EventModel/EventModel.h"
#include "EventModel/Event.h"

#include "EvtRecEvent/EvtRecEvent.h"
#include "EvtRecEvent/EvtRecTrack.h"
#include "DstEvent/TofHitStatus.h"
#include "EventModel/EventHeader.h"

#include "VertexFit/Helix.h"
#include "VertexFit/SecondVertexFit.h"
#include "VertexFit/KalmanKinematicFit.h"

#include "TMath.h"
#include "GaudiKernel/INTupleSvc.h"
#include "GaudiKernel/NTuple.h"
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IHistogramSvc.h"
#include "CLHEP/Vector/ThreeVector.h"
#include "CLHEP/Vector/LorentzVector.h"
#include "CLHEP/Vector/TwoVector.h"

#include "VertexFit/KalmanKinematicFit.h"
#include "VertexFit/VertexFit.h"
#include "VertexFit/Helix.h"
#include "ParticleID/ParticleID.h"
#include <vector>

#include "VertexFit/KalmanKinematicFit.h"
#include "VertexFit/VertexFit.h"
#include "VertexFit/Helix.h"
#include "ParticleID/ParticleID.h"

#include "MCTruthMatchSvc/MCTruthMatchSvc.h"

#include "TLorentzVector.h"
#include <TFile.h>
#include <TTree.h>
  
#include<fstream>
#include"McTruth/McParticle.h"  
using Event::McParticle;
using Event::McParticleCol;
using CLHEP::Hep3Vector;
using CLHEP::Hep2Vector;
using CLHEP::HepLorentzVector;

#include "McDecayModeSvc/McDecayModeSvc.h"

#include "CLHEP/Geometry/Point3D.h"
#ifndef ENABLE_BACKWARDS_COMPATIBILITY
   typedef HepGeom::Point3D<double> HepPoint3D;
#endif
typedef std::vector<int> Vint;
typedef std::vector<HepLorentzVector> Vp4;

const double mDs      = 1.9683;
const double mpion    = 0.13957;
const double mpi      = 0.13957;
const double mk       = 0.493677;
const double mp       = 0.938272;
const double mproton  = 0.938272;
const double mLbd     = 1.115683;
const double me       = 0.000511;
const double mpi0     = 0.134976; 
const double mLambda_c = 2.286;
const double mEta = 0.5478;


class Lc2PEtaprime : public Algorithm {

public:
    Lc2PEtaprime(const std::string& name, ISvcLocator* pSvcLocator);
    StatusCode initialize();
    StatusCode execute();
    StatusCode finalize();  

private:

    void MCtopo();
    //void MCtruth(SmartDataPtr<Event::McParticleCol> mcParticleCol);
    //bool angleMatch(int,vector<int>,vector<int>);

    int IsGoodTrack( vector<HepLorentzVector>& PionP,vector<int>& PionPIndex,  
                               vector<HepLorentzVector>& PionM, vector<int>& PionMIndex, 
                               vector<HepLorentzVector>& Proton, vector<int>& ProtonIndex,vector<int>& ProtonCharge);

    void IsGammatrack(vector<int>& Gam_Track, vector<HepLorentzVector>& Gam_P4,vector<double>&);

    void IsEta(vector<HepLorentzVector> Gam_EP,vector<int> Gam_Track,vector<double>,vector<HepLorentzVector>& Pi0_P4, double Pi0_shower[50][6]);

    int IsEtaprime(vector<int>,vector<HepLorentzVector>,
                   vector<int>,vector<HepLorentzVector>,
                   vector<HepLorentzVector>,double Eta_shower[50][6],
                   vector<HepLorentzVector>&,double EtaprimeInfo[50][8]);

    int IsLc(vector<HepLorentzVector>,vector<int>,vector<int>,
             vector<HepLorentzVector>,double EtaprimeInfo[50][8],
             double LcInfo[9]);

    double _PT(const HepLorentzVector&);

    double _P(const HepLorentzVector&);
    
    double _M(const HepLorentzVector& P);

    double _MBC(const HepLorentzVector& P);
    
    double _string2double(string input);
   
    SmartDataPtr<EvtRecEvent>* evtRecEvent;
    SmartDataPtr<EvtRecTrackCol>* evtRecTrkCol;
    SmartDataPtr<Event::McParticleCol>* mcParticleCol;

    string angleMatch_switch;

    TFile* outputFile;

    TTree* outputTree;
    TTree* outputTree_truth;
    
    double _beamE;

    int runNumber, eventNumber;
	int m_pdgid[100];
	int m_motheridx[100];
	int m_numParticle;

    int _charge;
    int _NTrk;
    int _NPp;
    int _NPm;
    int _NPip;
    int _NPim;
    int _NEta;
    int _NGamma;
    int _NEtaprime;
    double _Chisq;
    double _DeltaE;

    double _Lc_px;
    double _Lc_py;
    double _Lc_pz;
    double _Lc_pt;
    double _Lc_p;
    double _Lc_e;
    double _Lc_m;
    double _Lc_mbc;
 
    double _Etaprime_px;
    double _Etaprime_py;
    double _Etaprime_pz;
    double _Etaprime_pt;
    double _Etaprime_p;
    double _Etaprime_e;
    double _Etaprime_m;
  
    double _P_px;
    double _P_py;
    double _P_pz;
    double _P_pt;
    double _P_p;
    double _P_e;
 
    double _Pip_px;
    double _Pip_py;
    double _Pip_pz;
    double _Pip_pt;
    double _Pip_p;
    double _Pip_e;
 
    double _Pim_px;
    double _Pim_py;
    double _Pim_pz;
    double _Pim_pt;
    double _Pim_p;
    double _Pim_e;

    double _Eta_px;
    double _Eta_py;
    double _Eta_pz;
    double _Eta_pt;
    double _Eta_p;
    double _Eta_e;
    double _Eta_m;
    double _Eta_dangle1;
    double _Eta_dangle2;
    double _Eta_chisq;

    double _Gam1_px;
    double _Gam1_py;
    double _Gam1_pz;
    double _Gam1_pt;
    double _Gam1_p;
    double _Gam1_e;

    double _Gam2_px;
    double _Gam2_py;
    double _Gam2_pz;
    double _Gam2_pt;
    double _Gam2_p;
    double _Gam2_e;

    MCTruthMatchSvc * matchSvc;
    string matchSwitch;	

    // truth info :
   

};

#endif 
























