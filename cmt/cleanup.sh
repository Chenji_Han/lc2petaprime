# echo "cleanup Lc2PEtaprimeAlg Lc2PEtaprime-master in /workfs/bes/hancj/b705/workarea"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/bes3.ihep.ac.cn/bes3sw/ExternalLib/SLC6/contrib/CMT/v1r25; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtLc2PEtaprimeAlgtempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if test ! $? = 0 ; then cmtLc2PEtaprimeAlgtempfile=/tmp/cmt.$$; fi
${CMTROOT}/mgr/cmt cleanup -sh -pack=Lc2PEtaprimeAlg -version=Lc2PEtaprime-master -path=/workfs/bes/hancj/b705/workarea  $* >${cmtLc2PEtaprimeAlgtempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/mgr/cmt cleanup -sh -pack=Lc2PEtaprimeAlg -version=Lc2PEtaprime-master -path=/workfs/bes/hancj/b705/workarea  $* >${cmtLc2PEtaprimeAlgtempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmtLc2PEtaprimeAlgtempfile}
  unset cmtLc2PEtaprimeAlgtempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmtLc2PEtaprimeAlgtempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmtLc2PEtaprimeAlgtempfile}
unset cmtLc2PEtaprimeAlgtempfile
return $cmtcleanupstatus

