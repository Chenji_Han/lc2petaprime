# echo "cleanup Lc2PEtaprimeAlg Lc2PEtaprime-master in /workfs/bes/hancj/b705/workarea"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/bes3.ihep.ac.cn/bes3sw/ExternalLib/SLC6/contrib/CMT/v1r25
endif
source ${CMTROOT}/mgr/setup.csh
set cmtLc2PEtaprimeAlgtempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if $status != 0 then
  set cmtLc2PEtaprimeAlgtempfile=/tmp/cmt.$$
endif
${CMTROOT}/mgr/cmt cleanup -csh -pack=Lc2PEtaprimeAlg -version=Lc2PEtaprime-master -path=/workfs/bes/hancj/b705/workarea  $* >${cmtLc2PEtaprimeAlgtempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/mgr/cmt cleanup -csh -pack=Lc2PEtaprimeAlg -version=Lc2PEtaprime-master -path=/workfs/bes/hancj/b705/workarea  $* >${cmtLc2PEtaprimeAlgtempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmtLc2PEtaprimeAlgtempfile}
  unset cmtLc2PEtaprimeAlgtempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmtLc2PEtaprimeAlgtempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmtLc2PEtaprimeAlgtempfile}
unset cmtLc2PEtaprimeAlgtempfile
exit $cmtcleanupstatus

