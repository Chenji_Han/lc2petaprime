# echo "setup Lc2PEtaprimeAlg Lc2PEtaprime-master in /workfs/bes/hancj/b705/workarea"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/bes3.ihep.ac.cn/bes3sw/ExternalLib/SLC6/contrib/CMT/v1r25
endif
source ${CMTROOT}/mgr/setup.csh
set cmtLc2PEtaprimeAlgtempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if $status != 0 then
  set cmtLc2PEtaprimeAlgtempfile=/tmp/cmt.$$
endif
${CMTROOT}/mgr/cmt setup -csh -pack=Lc2PEtaprimeAlg -version=Lc2PEtaprime-master -path=/workfs/bes/hancj/b705/workarea  -no_cleanup $* >${cmtLc2PEtaprimeAlgtempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/mgr/cmt setup -csh -pack=Lc2PEtaprimeAlg -version=Lc2PEtaprime-master -path=/workfs/bes/hancj/b705/workarea  -no_cleanup $* >${cmtLc2PEtaprimeAlgtempfile}"
  set cmtsetupstatus=2
  /bin/rm -f ${cmtLc2PEtaprimeAlgtempfile}
  unset cmtLc2PEtaprimeAlgtempfile
  exit $cmtsetupstatus
endif
set cmtsetupstatus=0
source ${cmtLc2PEtaprimeAlgtempfile}
if ( $status != 0 ) then
  set cmtsetupstatus=2
endif
/bin/rm -f ${cmtLc2PEtaprimeAlgtempfile}
unset cmtLc2PEtaprimeAlgtempfile
exit $cmtsetupstatus

