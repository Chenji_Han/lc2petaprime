

#include "Lc2PEtaprimeAlg/Lc2PEtaprime.h"

double Lc2PEtaprime::_string2double(string input){
	
	int index = 0;
	double pre = 0.0;
	double aft = 0.0;
	bool comma = false;
	double num = 1.0;
	double charge;
	if(input[0]=='-') 
		charge = -1.0;
	else 
		charge = 1.0;

    bool scit = false;
    double suffix = 0;
    double factor = 0;

	while(input[index]!='\0'){
		if(input[index]=='.'){
			comma = true;
		}
		if('0'<=input[index]&&input[index]<='9'){
			double temp = input[index] - '0';
			if(comma==false){
				pre = pre*10.0 + temp;
			}else{
				num *= 10.0;
				aft += temp/num;
			}
		}
        if(input[index]=='e' || input[index] == 'E'){
            scit = true;
            suffix = pre + aft;
            factor = _string2double( input.substr( size_t(index+1) ) );
            break; 
        }
		index++;
	}

    if(scit)
	    return charge * suffix * pow( 10, factor ) ;
    else
        return charge * ( pre + aft) ;
}


double Lc2PEtaprime::_PT(const HepLorentzVector& P){

    return sqrt( P.px()*P.px() + P.py()*P.py() );

}

double Lc2PEtaprime::_P(const HepLorentzVector& P){

    return sqrt( P.px()*P.px() + P.py()*P.py() + P.pz()*P.pz() );

}

double Lc2PEtaprime::_M(const HepLorentzVector& P){

    return P.m();

}

double Lc2PEtaprime::_MBC(const HepLorentzVector& P){

    double temp = _beamE*_beamE - P.px()*P.px() - P.py()*P.py() - P.pz()*P.pz(); 
    return temp > 0 ? sqrt(temp) : -1.0 ;

}

///////////////////////////////////////////////////////////////

Lc2PEtaprime::Lc2PEtaprime(const std::string& name, ISvcLocator* pSvcLocator) :
  Algorithm(name, pSvcLocator){

}

StatusCode Lc2PEtaprime::initialize(){

    MsgStream log(msgSvc(), name());
    
    log << MSG::INFO << "in initialize()" << endmsg;
  
    // next few lines of code are used to read some parameters fromt the inputs file
    string outputName;
    string line;
    std::ifstream inputTxt("inputs.txt");
    while(getline(inputTxt, line)) {
            if (!line.length() || line[0] == '#')
                continue;
    
        std::istringstream iss(line);
            string a,b,c;
            iss>>a>>b>>c;
            if(a=="dst_outputName"){
                outputName = b;
            }else if(a=="dst_angleMatch"){
                angleMatch_switch = b;
            }else if(a=="beamE"){
                _beamE = _string2double(b);
            }
    
    }

    //next codes are used to initialize the match service
    //IMCTruthMatchSvc *i_matchSvc;
    //StatusCode sc_MC = service("MCTruthMatchSvc",i_matchSvc);
    //if(sc_MC.isFailure()){
    //	cout<<"MCTruthMatchSvc load fail"<<endl;
    //	return 0;
    //}
    //matchSvc=dynamic_cast<MCTruthMatchSvc*>(i_matchSvc);

    for(int i=0;i<100;i++){
        m_pdgid[i]=0;
        m_motheridx[i]=0;
    }
    m_numParticle = 0;    

    outputFile = new TFile(outputName.c_str(),"recreate");
    
    outputTree = new TTree("output","");
    outputTree_truth = new TTree("output_truth","");

    outputTree_truth -> Branch("indexmc",&m_numParticle);
    outputTree_truth -> Branch("pdgid",m_pdgid,"pdgid[100]/I");
    outputTree_truth -> Branch("motheridx",m_motheridx,"motheridx[100]/I");
    outputTree_truth -> Branch("runNumber",&runNumber);
    outputTree_truth -> Branch("eventNumber",&eventNumber);

    outputTree -> Branch("indexmc",&m_numParticle);
    outputTree -> Branch("pdgid",m_pdgid,"pdgid[100]/I");
    outputTree -> Branch("motheridx",m_motheridx,"motheridx[100]/I");
    outputTree -> Branch("runNumber",&runNumber);
    outputTree -> Branch("eventNumber",&eventNumber);
   
    outputTree -> Branch("charge",&_charge);
    outputTree -> Branch("DeltaE",&_DeltaE);
   
    outputTree -> Branch("NTrk",&_NTrk);
    outputTree -> Branch("NPp",&_NPp);
    outputTree -> Branch("NPm",&_NPm);
    outputTree -> Branch("NPip",&_NPip);
    outputTree -> Branch("NPim",&_NPim);
    outputTree -> Branch("NGamma",&_NGamma);
    outputTree -> Branch("NEta",&_NEta);
    outputTree -> Branch("NEtaprime",&_NEtaprime);
    
    outputTree -> Branch("Chisq2C",&_Chisq);
   
    outputTree -> Branch("Lc_px",&_Lc_px);
    outputTree -> Branch("Lc_py",&_Lc_py);
    outputTree -> Branch("Lc_pz",&_Lc_pz);
    outputTree -> Branch("Lc_pt",&_Lc_pt);
    outputTree -> Branch("Lc_p", &_Lc_p);
    outputTree -> Branch("Lc_e", &_Lc_e);
    outputTree -> Branch("Lc_m", &_Lc_m);
    outputTree -> Branch("Lc_mbc", &_Lc_mbc);

    outputTree -> Branch("Etaprime_px",&_Etaprime_px);
    outputTree -> Branch("Etaprime_py",&_Etaprime_py);
    outputTree -> Branch("Etaprime_pz",&_Etaprime_pz);
    outputTree -> Branch("Etaprime_pt",&_Etaprime_pt);
    outputTree -> Branch("Etaprime_p", &_Etaprime_p);
    outputTree -> Branch("Etaprime_e", &_Etaprime_e);
    outputTree -> Branch("Etaprime_m", &_Etaprime_m);

    outputTree -> Branch("P_px",&_P_px);
    outputTree -> Branch("P_py",&_P_py);
    outputTree -> Branch("P_pz",&_P_pz);
    outputTree -> Branch("P_pt",&_P_pt);
    outputTree -> Branch("P_p", &_P_p);
    outputTree -> Branch("P_e", &_P_e);

    outputTree -> Branch("Pim_px",&_Pim_px);
    outputTree -> Branch("Pim_py",&_Pim_py);
    outputTree -> Branch("Pim_pz",&_Pim_pz);
    outputTree -> Branch("Pim_pt",&_Pim_pt);
    outputTree -> Branch("Pim_p", &_Pim_p);
    outputTree -> Branch("Pim_e", &_Pim_e);

    outputTree -> Branch("Pip_px",&_Pip_px);
    outputTree -> Branch("Pip_py",&_Pip_py);
    outputTree -> Branch("Pip_pz",&_Pip_pz);
    outputTree -> Branch("Pip_pt",&_Pip_pt);
    outputTree -> Branch("Pip_p", &_Pip_p);
    outputTree -> Branch("Pip_e", &_Pip_e);

    outputTree -> Branch("Eta_px",&_Eta_px);
    outputTree -> Branch("Eta_py",&_Eta_py);
    outputTree -> Branch("Eta_pz",&_Eta_pz);
    outputTree -> Branch("Eta_pt",&_Eta_pt);
    outputTree -> Branch("Eta_p", &_Eta_p);
    outputTree -> Branch("Eta_e", &_Eta_e);
    outputTree -> Branch("Eta_m", &_Eta_m);
    outputTree -> Branch("Eta_dangle1", &_Eta_dangle1);
    outputTree -> Branch("Eta_dangle2", &_Eta_dangle2);
    outputTree -> Branch("Eta_Chisq", &_Eta_chisq);

    outputTree -> Branch("Gam1_px",&_Gam1_px);
    outputTree -> Branch("Gam1_py",&_Gam1_py);
    outputTree -> Branch("Gam1_pz",&_Gam1_pz);
    outputTree -> Branch("Gam1_pt",&_Gam1_pt);
    outputTree -> Branch("Gam1_p", &_Gam1_p);
    outputTree -> Branch("Gam1_e", &_Gam1_e);

    outputTree -> Branch("Gam2_px",&_Gam2_px);
    outputTree -> Branch("Gam2_py",&_Gam2_py);
    outputTree -> Branch("Gam2_pz",&_Gam2_pz);
    outputTree -> Branch("Gam2_pt",&_Gam2_pt);
    outputTree -> Branch("Gam2_p", &_Gam2_p);
    outputTree -> Branch("Gam2_e", &_Gam2_e);

    //outputTree -> Branch("KpKm",_KpKm,"KpKm[20][5]/D");
    //outputTree -> Branch("PipPi0",_PipPi0,"PipPi0[20][5]/D");
    //outputTree -> Branch("PimPi0",_PimPi0,"PimPi0[20][5]/D");
    //outputTree -> Branch("PipPim",_PipPim,"PipPim[20][5]/D");
 
  log << MSG::INFO << "successfully return from initialize()" <<endmsg;
  return StatusCode::SUCCESS;
  
}

int iEvt = 0;
//***********************************************************************
StatusCode Lc2PEtaprime::execute() {

    bool debug = true;

    if( (iEvt++)%5000==0) cout<<iEvt<<endl;

    MsgStream log(msgSvc(), name());
    log << MSG::INFO << "in execute()" << endreq;

    SmartDataPtr<Event::EventHeader> eventHeader(eventSvc(),"/Event/EventHeader");
    
    SmartDataPtr<Event::McParticleCol> mcParticleCol_(eventSvc(),"/Event/MC/McParticleCol");
    mcParticleCol = &mcParticleCol_;
    SmartDataPtr<EvtRecEvent> evtRecEvent_(eventSvc(), EventModel::EvtRec::EvtRecEvent);
    evtRecEvent = &evtRecEvent_;
    SmartDataPtr<EvtRecTrackCol> evtRecTrkCol_(eventSvc(),  EventModel::EvtRec::EvtRecTrackCol);
    evtRecTrkCol = &evtRecTrkCol_;

    runNumber=eventHeader->runNumber();
    eventNumber=eventHeader->eventNumber();
    
    if( runNumber<0 ){

        MCtopo();
        //MCtruth();
    }

    vector<int> PionPIndex; PionPIndex.clear();
	vector<int> PionMIndex; PionMIndex.clear();
    vector<int> ProtonIndex; ProtonIndex.clear();
	vector<int> ProtonCharge; ProtonCharge.clear();

    vector<HepLorentzVector> PionPinfo; PionPinfo.clear();
	vector<HepLorentzVector> PionMinfo; PionMinfo.clear();
    vector<HepLorentzVector> ProtonInfo; ProtonInfo.clear();

	_NTrk = IsGoodTrack(PionPinfo,PionPIndex,
                                                PionMinfo,PionMIndex,
                                                ProtonInfo,ProtonIndex,ProtonCharge);
   
    _NPp = 0; _NPm = 0;
    for(int i=0; i<ProtonCharge.size(); i++){
        if( ProtonCharge[i] == 1 ){
            _NPp ++ ;
        }else{
            _NPm ++ ;
        }
    }
    _NPip = PionPIndex.size(); _NPim = PionMIndex.size();

    if(debug){
        cout<<"num of P^{+}: "<<_NPp<<endl;
        cout<<"num of P^{-}: "<<_NPm<<endl;
        cout<<"num of pi^{+}: "<<_NPip<<endl;
        cout<<"num of pi^{-}: "<<_NPim<<endl;
    }
    //cout<<"KaonP "<<KaonPIndex.size()<<" PionM "<<PionMIndex.size()<<endl;

    vector<int> Gam_Track; Gam_Track.clear();
    vector<HepLorentzVector> Gam_P4; Gam_P4.clear();
    vector<double> Gam_dangle; Gam_dangle.clear();
    IsGammatrack(Gam_Track, Gam_P4, Gam_dangle);
    _NGamma = Gam_Track.size();
    //cout<<"num of gamma "<<Gam_Track.size()<<endl;   
    if(debug){
        cout<<"num of gamma: "<<_NGamma<<endl;
    }
    
    vector<HepLorentzVector> Eta_P4; Eta_P4.clear();
    double Eta_shower[50][6];
    for(int i=0;i<50;i++){
        for(int j=0;j<6;j++){
            Eta_shower[i][j] = -1.0;
        }
    }

    IsEta(Gam_P4, Gam_Track, Gam_dangle, Eta_P4, Eta_shower);
    _NEta = Eta_P4.size();

    if(debug){
        cout<<"num of Eta: "<<_NEta<<endl;
    }
    
    //cout<<"num of Pi0 "<<Pi0_P4.size()<<endl;   

    vector<HepLorentzVector> EtaprimeP4; EtaprimeP4.clear();
    double EtaprimeInfo[50][8];
    for (int i=0; i<50; i++){
        for (int j=0; j<8; j++){
            EtaprimeInfo[i][j] = -1.0;
        }
    }

    _NEtaprime = IsEtaprime(PionPIndex, PionPinfo,
                            PionMIndex, PionMinfo,
                            Eta_P4, Eta_shower,
                            EtaprimeP4, EtaprimeInfo);

    if(debug){
        cout<<"num of Etaprime: "<<_NEtaprime<<endl;
    }
    
    double LcInfo[9];
    for ( int i=0; i<9; i++ ){
        LcInfo[9] = -1.0;
    }
    _charge = IsLc(ProtonInfo, ProtonIndex, ProtonCharge,
                   EtaprimeP4, EtaprimeInfo,
                   LcInfo);
 
    if(debug){
        cout<<"charge of Lc: "<<_charge<<endl;
        cout<<endl;
    }
 
    if( _charge == 0 ){
        return StatusCode::SUCCESS;
    }

    _DeltaE = LcInfo[9];

    EvtRecTrackIterator TrkP = (*evtRecTrkCol)->begin() + LcInfo[0];
    RecMdcTrack* mdcTrkP = (*TrkP)->mdcTrack();

    EvtRecTrackIterator TrkPip = (*evtRecTrkCol)->begin() + LcInfo[1];
    RecMdcTrack* mdcTrkPip = (*TrkPip)->mdcTrack();
  
    EvtRecTrackIterator TrkPim = (*evtRecTrkCol)->begin() + LcInfo[2];
    RecMdcTrack* mdcTrkPim = (*TrkPim)->mdcTrack();
  
    EvtRecTrackIterator TrkEta1 = (*evtRecTrkCol)->begin() + LcInfo[3];
    RecEmcShower* emcTrkEta1 = (*TrkEta1)->emcShower();

    EvtRecTrackIterator TrkEta2 = (*evtRecTrkCol)->begin() + LcInfo[4];
    RecEmcShower* emcTrkEta2 = (*TrkEta2)->emcShower();

    KalmanKinematicFit * kfit_ = KalmanKinematicFit::instance();
	kfit_->init();
	kfit_->AddTrack(0, mproton,  mdcTrkP);
	kfit_->AddTrack(1, mpi, mdcTrkPip);
	kfit_->AddTrack(2, mpi, mdcTrkPim);
	kfit_->AddTrack(3, 0,   emcTrkEta1);
	kfit_->AddTrack(4, 0,   emcTrkEta2);
    kfit_->AddMissTrack(5, mLambda_c);

    kfit_->AddFourMomentum( 0, HepLorentzVector(_beamE*2*0.011,0,0,_beamE*2) );
    kfit_->AddResonance(1,mEta,3,4);

    if( !(kfit_->Fit()) ){
        return StatusCode::SUCCESS;
    }

    _Chisq = kfit_->chisq();

    HepLorentzVector PP4 = kfit_->pfit(0); 
    PP4.boost(-0.011,0,0);
    HepLorentzVector PipP4 = kfit_->pfit(1); 
    PipP4.boost(-0.011,0,0);
    HepLorentzVector PimP4 = kfit_->pfit(2); 
    PimP4.boost(-0.011,0,0);
    HepLorentzVector EtaP4 = kfit_->pfit(3) + kfit_ -> pfit(4);
    EtaP4.boost(-0.011,0,0);
    HepLorentzVector Gam1P4 = kfit_->pfit(3);
    Gam1P4.boost(-0.011,0,0);
    HepLorentzVector Gam2P4 = kfit_->pfit(4);
    Gam2P4.boost(-0.011,0,0);

    HepLorentzVector LcP4_ = PP4 + PipP4 + PimP4 + EtaP4;
    HepLorentzVector EtaprimeP4_ = PipP4 + PimP4 + EtaP4;

    _Lc_px = LcP4_.px();
    _Lc_py = LcP4_.py();
    _Lc_pz = LcP4_.pz();
    _Lc_e  = LcP4_.e();
    _Lc_pt = _PT(LcP4_);
    _Lc_p  =  _P(LcP4_);
    _Lc_m  =  _M(LcP4_);
    _Lc_mbc  =  _MBC(LcP4_);

    _Etaprime_px = EtaprimeP4_.px();
    _Etaprime_py = EtaprimeP4_.py();
    _Etaprime_pz = EtaprimeP4_.pz();
    _Etaprime_e  = EtaprimeP4_.e();
    _Etaprime_pt = _PT(EtaprimeP4_);
    _Etaprime_p  =  _P(EtaprimeP4_);
    _Etaprime_m  =  _M(EtaprimeP4_);

    _P_px = PP4.px();
    _P_py = PP4.py();
    _P_pz = PP4.pz();
    _P_e  = PP4.e();
    _P_pt = _PT(PP4);
    _P_p  =  _P(PP4);

    _Pip_px = PipP4.px();
    _Pip_py = PipP4.py();
    _Pip_pz = PipP4.pz();
    _Pip_e  = PipP4.e();
    _Pip_pt = _PT(PipP4);
    _Pip_p  =  _P(PipP4);

    _Pim_px = PimP4.px();
    _Pim_py = PimP4.py();
    _Pim_pz = PimP4.pz();
    _Pim_e  = PimP4.e();
    _Pim_pt = _PT(PimP4);
    _Pim_p  =  _P(PimP4);

    _Eta_px = EtaP4.px();
    _Eta_py = EtaP4.py();
    _Eta_pz = EtaP4.pz();
    _Eta_e  = EtaP4.e();
    _Eta_pt = _PT(EtaP4);
    _Eta_p  =  _P(EtaP4);
    _Eta_m  =  LcInfo[5];
    _Eta_dangle1  =  LcInfo[6];
    _Eta_dangle2  =  LcInfo[7];
    _Eta_chisq  =  LcInfo[8];

    _Gam1_px = Gam1P4.px();
    _Gam1_py = Gam1P4.py();
    _Gam1_pz = Gam1P4.pz();
    _Gam1_e  = Gam1P4.e();
    _Gam1_pt = _PT(Gam1P4);
    _Gam1_p  =  _P(Gam1P4);

    _Gam2_px = Gam2P4.px();
    _Gam2_py = Gam2P4.py();
    _Gam2_pz = Gam2P4.pz();
    _Gam2_e  = Gam2P4.e();
    _Gam2_pt = _PT(Gam2P4);
    _Gam2_p  =  _P(Gam2P4);

    outputTree->Fill();
    outputTree_truth->Fill();
    
    if(debug){
        cout<<"pass"<<endl;
    }

    return StatusCode::SUCCESS;
} //end of execute()



// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
StatusCode Lc2PEtaprime::finalize() {
  MsgStream log(msgSvc(), name());
  log << MSG::INFO << "in finalize()" << endmsg;

    outputFile->cd();
    outputTree_truth->Write();
    outputTree->Write();
    outputFile->Close();

  return StatusCode::SUCCESS;
}






