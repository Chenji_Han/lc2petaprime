/*************************************************************************
    > File Name: IsLc.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: 2020年07月23日 星期四 10时00分29秒
 ************************************************************************/


#include "Lc2PEtaprimeAlg/Lc2PEtaprime.h"

int Lc2PEtaprime::IsLc(vector<HepLorentzVector> ProtonInfo, vector<int> ProtonIndex, vector<int> ProtonCharge,
                        vector<HepLorentzVector> EtaprimeP4, double EtaprimeInfo[50][8],
                        double LcInfo[9]){

    double bestDE = -999;
    bool pass = false;
    int charge = 0;
    int P, Etaprime;

    for(int i=0; i<ProtonIndex.size(); i++){
        for(int j=0; j<EtaprimeP4.size(); j++){
            if( ProtonIndex[i] == EtaprimeInfo[j][0] || ProtonIndex[i] == EtaprimeInfo[j][1] ) continue;
            HepLorentzVector temp = ProtonInfo[i] + EtaprimeP4[j];
            temp.boost(-0.011,0,0);
            double deltaE = _beamE - temp.e();
            if ( fabs(deltaE) < fabs(bestDE) ){
                pass = true;
                charge = ProtonCharge[i];
                bestDE = deltaE;
                P = i; Etaprime = j;
            }
        }
    }

    if ( pass ){
       
        LcInfo[0] = ProtonIndex[P]; // id of P
        LcInfo[1] = EtaprimeInfo[Etaprime][0]; // id of \pi^+
        LcInfo[2] = EtaprimeInfo[Etaprime][1]; // id of \pi^-
        LcInfo[3] = EtaprimeInfo[Etaprime][2]; // id of \gamma 1
        LcInfo[4] = EtaprimeInfo[Etaprime][3]; // id of \gamma 2
        LcInfo[5] = EtaprimeInfo[Etaprime][4]; // original mass of \Eta
        LcInfo[6] = EtaprimeInfo[Etaprime][5]; // \Delta angle of \gamma 1
        LcInfo[7] = EtaprimeInfo[Etaprime][6]; // \Delta angle of \gamma 2
        LcInfo[8] = EtaprimeInfo[Etaprime][7]; // chsq of \Eta mass 1C
        LcInfo[9] = bestDE; // chsq of \Eta mass 1C

        return charge;
    }else{

        return 0;
    }


}


