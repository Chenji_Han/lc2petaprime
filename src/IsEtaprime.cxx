/*************************************************************************
    > File Name: IsEtaprime.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: 2020年07月23日 星期四 09时12分53秒
 ************************************************************************/


#include "Lc2PEtaprimeAlg/Lc2PEtaprime.h"


int Lc2PEtaprime::IsEtaprime(vector<int> PionPIndex,vector<HepLorentzVector> PionPinfo,
                             vector<int> PionMIndex,vector<HepLorentzVector> PionMinfo,
                             vector<HepLorentzVector> Eta_P4, double Eta_shower[50][6],
                             vector<HepLorentzVector>& EtaprimeP4, double EtaprimeInfo[50][8]){


    int NEtaprime = 0;
    for(int i=0;i<PionPinfo.size();i++){
        for(int j=0;j<PionMinfo.size();j++){
            for(int k=0;k<Eta_P4.size();k++){
             
                HepLorentzVector Etaprimetemp = PionPinfo[i] + PionMinfo[j] + Eta_P4[k] ;
                if(!(0.93<Etaprimetemp.m()&&Etaprimetemp.m()<0.98)) continue;
                EtaprimeP4.push_back(Etaprimetemp);
                EtaprimeInfo[NEtaprime][0] = PionPIndex[i];
                EtaprimeInfo[NEtaprime][1] = PionMIndex[j];
                EtaprimeInfo[NEtaprime][2] = Eta_shower[k][0];// gamma trk 1
                EtaprimeInfo[NEtaprime][3] = Eta_shower[k][1];// gamma trk 2
                EtaprimeInfo[NEtaprime][4] = Eta_shower[k][2];// original eta mass
                EtaprimeInfo[NEtaprime][5] = Eta_shower[k][3];// delta angle of trk1 
                EtaprimeInfo[NEtaprime][6] = Eta_shower[k][4];// delta angle of trk2 
                EtaprimeInfo[NEtaprime][7] = Eta_shower[k][5];// chisq of 1C 
                NEtaprime ++ ;
             
            }// for looping Eta
        }// for looping Pim
    }// for looing Pip


    return NEtaprime;

}

