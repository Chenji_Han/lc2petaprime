#include "GaudiKernel/DeclareFactoryEntries.h"
#include "Lc2PEtaprimeAlg/Lc2PEtaprime.h"

DECLARE_ALGORITHM_FACTORY( Lc2PEtaprime )

DECLARE_FACTORY_ENTRIES( Lc2PEtaprimeAlg ) {
  DECLARE_ALGORITHM(Lc2PEtaprime);
}

